import { StyleSheet, Text, View, TouchableOpacity, Image } from 'react-native';
import React from 'react';
import { Board } from './Board'
import { UserName } from './Settings';

export class Game extends React.Component {
  // Имена игроков по часовой стрелке
  playerNames = ['D', 'N', 'S', 'M']

  // Конец игры
  gameOver(text) {
    this.setState({
      disableButtons: true,
      text: text,
      spectator: true,
      ended: true
    }, () => console.log("Game ended"))
  }

  // Установка текста в поле информации
  setText(text) {
    this.setState({
      text: text,
    })
  }


  constructor(props) {
    super(props);

    let squaress = []
    let dead = []
    let meat = [true, true, true, true]

    squaress = [
      'D', 'D', 'D', null, null, null,
      'D', 'D', null, null, null, null,
      'D', null, null, null, null, null,
      null, null, null, null, null, 'S',
      null, null, null, null, 'S', 'S',
      null, null, null, 'S', 'S', 'S',
    ]
    dead = [false, true, false, true]
    meat = [true, false, false, false]  

    this.state = {
      // Игровое поле
      squares: squaress,
      // Состояние игроков
      dead: dead,
      // Исходное поле (нужно для ресета)
      squaresDefault: [...squaress],
      // Исходное состояниее игроков (нужно для ресета)
      deadDefault: dead,
      // Говорит, взял ли уже игрок шашку
      handling: false,
      // Нынешний игрок
      currentPlayer: 0,
      // Нынешняя активная клетка (индекс)
      currentActiveCell: null,
      // Нынешняя активная шашка
      currentCheck: 'D',
      // Лок на смену активной шашкм (когда игрок пошел в атаку)
      lock: false,
      // Отключение кнопок
      disableButtons: false,
      // Кто из игроков является человеком
      meat: meat,
      // Закончилась ли игра
      ended: false,
    };
  }

  // Сброс доски в изначальное положение (доступен только в офлайне)
  reset() {
    this.setState({
      squares: [...this.state.squaresDefault],
      dead: [...this.state.deadDefault],
      handling: false,
      currentPlayer: 0,
      currentActiveCell: null,
      currentCheck: 'D',
      lock: false,
      ended: false
    })
  }

  // Обработка хода бота
  useSkynet() {
    // Ставим таймаут для того, чтобы пользователь не считал себя медленным
    setTimeout(() => {

      let squares = this.state.squares
      let myChecks = []
      let player = this.state.currentPlayer % 4
      let hardmode = this.state.harderBots

      // Получаем информацию об индексах шашек ИИ
      for (let i = 0; i < squares.length; i++) {
        if (squares[i] && squares[i].charAt(0) === this.playerNames[this.state.currentPlayer % 4]) {
          myChecks[myChecks.length] = i
        }
      }
      console.log('[' + player + "] check indexes: " + myChecks)

      let movesStart = []
      let movesEnd = []
      let oneScoreStart = []
      let oneScoreEnd = []
      let badMovesStart = []
      let badMovesEnd = []
      let lock = false

      // Проходимся по всем шашкам ИИ
      for (let i = 0; i < myChecks.length; i++) {
        let score = 0

        // Ходы для дамки
        if (squares[myChecks[i]].endsWith('Q')) {
          // Влево
          for (let q = myChecks[i] - 1; q >= Math.floor(myChecks[i] / 6) * 6; q--) {
            if (squares[q] !== null) {
              if (squares[q].charAt(0) !== squares[myChecks[i]].charAt(0) && squares[q - 1] === null && q !== Math.floor(myChecks[i] % 6) * 6) {
                squares[q - 1] = 'V'
                score = 2
              }
              break
            } else {
              squares[q] = 'v'
              score = score > 1 ? 2 : 1
            }
          }
          // Вправо
          for (let q = myChecks[i] + 1; q <= Math.floor(myChecks[i] / 6) * 6 + 5; q++) {
            if (squares[q] !== null) {
              if (squares[q].charAt(0) !== squares[myChecks[i]].charAt(0) && squares[q + 1] === null && q !== Math.floor(myChecks[i] % 6) * 6 + 5) {
                squares[q + 1] = 'V'
                score = 2
              }
              break
            } else {
              squares[q] = 'v'
              score = score > 1 ? 2 : 1
            }
          }
          // Вверх
          for (let q = myChecks[i] - 6; q > 0; q -= 6) {
            if (squares[q] !== null) {
              if (squares[q].charAt(0) !== squares[myChecks[i]].charAt(0) && squares[q - 6] === null && q !== myChecks[i] % 6) {
                squares[q - 6] = 'V'
                score = 2
              }
              break
            } else {
              squares[q] = 'v'
              score = score > 1 ? 2 : 1
            }
          }
          // Вниз
          for (let q = myChecks[i] + 6; q < 36; q += 6) {
            if (squares[q] !== null) {
              if (squares[q].charAt(0) !== squares[myChecks[i]].charAt(0) && squares[q + 6] === null && q !== 30 + myChecks[i] % 6) {
                squares[q + 6] = 'V'
                score = 2
              }
              break
            } else {
              squares[q] = 'v'
              score = score > 1 ? 2 : 1
            }
          }
        }
        // Ходы для обычной шашки
        else {
          // Вправо
          if (squares[myChecks[i] + 1]) {
            if (squares[myChecks[i] + 2] === null && squares[myChecks[i]].charAt(0) !== squares[myChecks[i] + 1].charAt(0) && Math.floor((myChecks[i] + 2) / 6) === Math.floor(myChecks[i] / 6)) {
              squares[myChecks[i] + 2] = 'V'
              score = 2
            }
          }
          else {
            if (myChecks[i] < 35 && Math.floor((myChecks[i] + 1) / 6) === Math.floor(myChecks[i] / 6) && (player === 0 || player === 3)) {
              if (hardmode && myChecks[i] < 33 && squares[myChecks[i] + 2] && squares[myChecks[i] + 3] && squares[myChecks[i]].charAt(0) !== squares[myChecks[i] + 2] && squares[myChecks[i]].charAt(0) !== squares[myChecks[i] + 3]) {
                squares[myChecks[i] + 1] = 'vb'
              } else {
                squares[myChecks[i] + 1] = 'v'
              }
              score = score > 1 ? 2 : 1
            }
          }

          // Вниз
          if (squares[myChecks[i] + 6]) {
            if (squares[myChecks[i] + 12] === null && squares[myChecks[i]].charAt(0) !== squares[myChecks[i] + 6].charAt(0) && myChecks[i] + 6 < 30 && myChecks[i] + 6 > 5) {
              squares[myChecks[i] + 12] = 'V'
              score = 2
            }
          }
          else {
            if (myChecks[i] < 30 && (player === 0 || player === 1)) {
              if (hardmode && myChecks[i] < 18 && squares[myChecks[i] + 12] && squares[myChecks[i] + 18] && squares[myChecks[i]].charAt(0) !== squares[myChecks[i] + 12] && squares[myChecks[i]].charAt(0) !== squares[myChecks[i] + 18]) {
                squares[myChecks[i] + 6] = 'vb'
              } else {
                squares[myChecks[i] + 6] = 'v'
              }
              score = score > 1 ? 2 : 1
            }
          }

          // Влево
          if (squares[myChecks[i] - 1]) {
            if (squares[myChecks[i] - 2] === null && squares[myChecks[i]].charAt(0) !== squares[myChecks[i] - 1].charAt(0) && Math.floor((myChecks[i] - 2) / 6) === Math.floor(myChecks[i] / 6)) {
              squares[myChecks[i] - 2] = 'V'
              score = 2
            }
          }
          else {
            if (myChecks[i] > 0 && Math.floor((myChecks[i] - 1) / 6) === Math.floor(myChecks[i] / 6) && (player === 1 || player === 2)) {
              if (hardmode && myChecks[i] > 2 && squares[myChecks[i] - 2] && squares[myChecks[i] - 3] && squares[myChecks[i]].charAt(0) !== squares[myChecks[i] - 2] && squares[myChecks[i]].charAt(0) !== squares[myChecks[i] - 3]) {
                squares[myChecks[i] - 1] = 'vb'
              } else {
                squares[myChecks[i] - 1] = 'v'
              }
              score = score > 1 ? 2 : 1
            }
          }

          // Вверх
          if (squares[myChecks[i] - 6]) {
            if (squares[myChecks[i] - 12] === null && squares[myChecks[i]].charAt(0) !== squares[myChecks[i] - 6].charAt(0) && myChecks[i] - 6 < 30 && myChecks[i] - 6 > 5) {
              squares[myChecks[i] - 12] = 'V'
              score = 2
            }
          }
          else {
            if (myChecks[i] > 5 && (player === 2 || player === 3)) {
              if (myChecks[i] > 17 && squares[myChecks[i] - 12] && squares[myChecks[i] - 18] && squares[myChecks[i]].charAt(0) !== squares[myChecks[i] - 12] && squares[myChecks[i]].charAt(0) !== squares[myChecks[i] - 18]) {
                squares[myChecks[i] - 6] = 'vb'
              } else {
                squares[myChecks[i] - 6] = 'v'
              }
              score = score > 1 ? 2 : 1
            }
          }
        }

        // Если не можем сдвинуться
        if (score === 0) {
          continue
        }

        let bestMoves = []
        let badMoves = []

        // Собираем возможные комбинации ходов
        for (let j = 0; j < squares.length; j++) {
          if (score === 1 && squares[j] === 'v') {
            bestMoves[bestMoves.length] = j
          }
          if (score === 2 && squares[j] === 'V') {
            bestMoves[bestMoves.length] = j
          }
          if (squares[j] === 'vb') {
            badMoves[badMoves.length] = j
          }
        }
        // Если можем сделать нормальный ход
        if (bestMoves.length !== 0) {
          // Если это отличный ход
          if (score === 2) {
            movesStart[movesStart.length] = myChecks[i]
            movesEnd[movesEnd.length] = bestMoves[Math.floor(Math.random() * bestMoves.length)]
          } else {
            // Если это хороший ход
            oneScoreStart[oneScoreStart.length] = myChecks[i]
            oneScoreEnd[oneScoreEnd.length] = bestMoves[Math.floor(Math.random() * bestMoves.length)]
          }
        } else {
          // Если вариантов не осталось и пришлось подставиться под противника
          if (badMoves.length !== 0) {
            badMovesStart[badMovesStart.length] = myChecks[i]
            badMovesEnd[badMovesEnd.length] = badMoves[Math.floor(Math.random() * badMoves.length)]
          }
        }

        // Сбрасываем стол к исходному состоянию
        squares = squares.map(x => x ? (x.toLocaleLowerCase().includes('v') ? null : x) : null)
      }

      // Если можем двинутся и кого-то съесть
      if (movesStart.length > 0) {
        // Выбираем рандомный ход и отправляем его на исполнение
        let rnd = Math.floor(Math.random() * movesStart.length)
        squares[movesStart[rnd]] = squares[movesStart[rnd]].toLocaleLowerCase()
        this.setState({
          currentActiveCell: movesStart[rnd],
          currentCheck: squares[movesStart[rnd]].toUpperCase(),
          currentPlayer: player,
          squares: this.checkMovement(squares, movesStart[rnd], false),
          handling: true,
        }, () => {
          let move = this.handleClick(movesEnd[rnd])
          let timerId = setInterval(() => {
            if ((move && move.includes('v'))) {
              move = this.handleClick(move.findIndex(x => x && x === 'v'))
            } else {
              clearInterval(timerId)
            }
          }, 250);
        })
        console.log("[" + player + "] AI HAS MADE A GOOD MOVE")
        return
      }
      // Если никого съесть не можем, но переместиться сумеем
      if (oneScoreStart.length > 0) {
        let rnd = Math.floor(Math.random() * oneScoreStart.length)
        squares[oneScoreStart[rnd]] = squares[oneScoreStart[rnd]].toLocaleLowerCase()
        this.setState({
          currentActiveCell: oneScoreStart[rnd],
          currentCheck: squares[oneScoreStart[rnd]].toUpperCase(),
          currentPlayer: player,
          squares: this.checkMovement(squares, oneScoreStart[rnd], false),
          handling: true,
        }, () => this.handleClick(oneScoreEnd[rnd]))
        console.log("[" + player + "] AI HAS MADE A NORMAL MOVE")
        return
      }
      // Если единственный выход - подставится
      if (badMovesStart.length > 0) {
        let rnd = Math.floor(Math.random() * badMovesStart.length)
        squares[badMovesStart[rnd]] = squares[badMovesStart[rnd]].toLocaleLowerCase()
        this.setState({
          currentActiveCell: badMovesStart[rnd],
          currentCheck: squares[badMovesStart[rnd]].toUpperCase(),
          currentPlayer: player,
          squares: this.checkMovement(squares, badMovesStart[rnd], false),
          handling: true,
        }, () => this.handleClick(badMovesEnd[rnd]))
        console.log("[" + player + "] AI WAS DESPERATE TO MAKE A BAD MOVE")
        return
      }

      // Если не можем двинуться, то пропускаем ход
      console.log("[" + player + "] AI CANT MOVE")
      this.skipTurn()

    }, 250)
  }


  // Обработка нажатия на клетку игрового поля
  handleClick(i) {
    // Если игра закончилась
    if (this.state.ended) {
      return
    }

    // // Доска
    let squares = this.state.squares
    // Если ткнули на пустую клетку
    if (!squares[i]) {
      return
    }

    // Если игрок еще не выбрал шашку и ткнул на одну из своих
    if (squares[i].charAt(0) === this.playerNames[this.state.currentPlayer % 4] && !this.state.handling) {
      this.setState({
        currentCheck: squares[i],
      })
      // Хайлайтим ходы для выбранной шашки
      squares[i] = squares[i].toLocaleLowerCase()
      squares = this.checkMovement(squares, i, false)
      this.setState({
        currentActiveCell: i,
        handling: true,
        squares: squares
      })


    } else {
      // Если игрок уже тыкал на другую шашку и хочет ее сменить, при этом не ходя предыдущей
      if (!this.state.lock && this.state.handling && squares[i].charAt(0) === squares[this.state.currentActiveCell].charAt(0).toUpperCase()) {
        // Возвращаем предыдущую к состоянию до хайлайтов
        squares = this.reverseSelection(squares)

        // Запоминаем новую шашку
        this.setState({
          currentActiveCell: i,
          currentCheck: squares[i],
          handling: true,
          squares: squares
        })

        // Смотрим возможные ходы для новой
        squares[i] = squares[i].toLocaleLowerCase()
        squares = this.checkMovement(squares, i, false)
      }
    }

    // Если игрок выбрал шашку и ткнул в один из хайлайтов
    if (this.state.handling && squares[i] === 'v') {
      // Заменяем хайлайт на выбранную шашку
      squares[i] = this.state.currentCheck

      // Смотрим, можно ли продолжить ход, также удаляем фишки, стоявшие на пути игрока
      let resume = false
      let eaten = false
      if (this.state.currentActiveCell % 6 < i % 6 && Math.abs(this.state.currentActiveCell - i) > 1) {
        if (squares[i - 1] !== 'v') {
          eaten = true
          squares[i - 1] = null
        }
        resume = true
      }
      if (Math.floor(this.state.currentActiveCell / 6) < Math.floor(i / 6) && this.state.currentActiveCell % 6 === i % 6 && Math.abs(this.state.currentActiveCell - i) > 6) {
        if (squares[i - 6] !== 'v') {
          eaten = true
          squares[i - 6] = null
        }
        resume = true
      }
      if (this.state.currentActiveCell % 6 > i % 6 && Math.abs(this.state.currentActiveCell - i) > 1) {
        if (squares[i + 1] !== 'v') {
          eaten = true
          squares[i + 1] = null
        }
        resume = true
      }
      if (Math.floor(this.state.currentActiveCell / 6) > Math.floor(i / 6) && this.state.currentActiveCell % 6 === i % 6 && Math.abs(this.state.currentActiveCell - i) > 6) {
        if (squares[i + 6] !== 'v') {
          eaten = true
          squares[i + 6] = null
        }
        resume = true
      }

      // Смотрим, можно ли сделать из шашки дамку
      squares = this.makeQueen(this.validate(squares), i)

      // Запоминаем положение доски и скидываем лок на выбор другой шашки
      this.setState({
        squares: squares,
        handling: false,
        lock: false,
      })


      // Если можно продолжить
      if (eaten && resume && this.canResume(squares, i, this.state.currentCheck)) {
        // Хайлайтим возможные ходы
        squares = this.checkMovement(squares, i, true)
        this.setState({
          currentCheck: squares[i],
        })
        squares[i] = squares[i].toLocaleLowerCase()
        this.setState({
          squares: squares,
          currentPlayer: this.state.currentPlayer,
          handling: true,
          currentActiveCell: i,
          lock: true,
        })

      } else {
        // Если продолжить нельзя
        this.chechAlive(squares)
        this.setState({
          squares: this.skipTurn(squares)
        })


      }
    }
    return squares
  }

  // Проверка на жизнеспособность игрока
  chechAlive(squares) {
    let ded = this.state.dead
    for (let p = 0; p < 4; p++) {
      let playerDead = true
      for (let i = 0; i < 36; i++) {
        if (squares[i] && squares[i].charAt(0).toUpperCase() === this.playerNames[p]) {
          playerDead = false
        }
      }
      ded[p] = playerDead
    }
    this.setState({
      dead: ded,
    })
  }

  // Смотрим, можно ли создать дамку
  makeQueen(squares, i) {
    // Если шашка уже дамка
    if (this.state.currentCheck.length !== 1) {
      return squares
    }
    // Смотрим на углы поля
    switch (this.state.currentCheck) {
      case this.playerNames[0]: {
        if (i == 35) {
          squares[35] = this.state.currentCheck + "Q"
        }
        break
      }
      case this.playerNames[1]: {
        if (i == 30) {
          squares[30] = this.state.currentCheck + "Q"
        }
        break
      }
      case this.playerNames[2]: {
        if (i == 0) {
          squares[0] = this.state.currentCheck + "Q"
        }
        break
      }
      case this.playerNames[3]: {
        if (i == 5) {
          squares[5] = this.state.currentCheck + "Q"
        }
        break
      }
    }
    // Возвращаем обработанную доску
    return squares
  }

  // Смотрим, можно ли кого-нибудь съесть с нынешним расположением шашки игрока
  canResume(squares, i, check) {
    let left = true, right = true, up = true, down = true

    // Проверяем положение шашки на поле
    if (i < 5) {
      up = false
    }
    if (i > 28) {
      down = false
    }
    if ((i % 6) < 2) {
      left = false
    }
    if ((i % 6) > 3) {
      right = false
    }

    // Проверяем возможность съесть шашку противника 
    if (!(up && squares[i - 6] && squares[i - 6].charAt(0) !== check.charAt(0) && squares[i - 12] === null)) {
      up = false
    }
    if (!(down && squares[i + 6] && squares[i + 6].charAt(0) !== check.charAt(0) && squares[i + 12] === null)) {
      down = false
    }
    if (!(left && squares[i - 1] && squares[i - 1].charAt(0) !== check.charAt(0) && squares[i - 2] === null)) {
      left = false
    }
    if (!(right && squares[i + 1] && squares[i + 1].charAt(0) !== check.charAt(0) && squares[i + 2] === null)) {
      right = false
    }
    return (up || down || right || left)
  }

  // Убираем все хайлайты
  // returns : игровое поле
  validate(squares) {
    return squares.map(x => x ? (x !== x.toLocaleLowerCase() ? x : null) : null)
  }

  // Обработка ситуации, когда игрок решил сдаться
  giveUp() {
    let ded = this.state.dead
    ded[this.state.currentPlayer % 4] = true
    this.setState({
      dead: ded,
      squares: this.state.squares.map(x => x ? (x.charAt(0).toUpperCase() === this.playerNames[this.state.currentPlayer % 4] ? null : x) : null)
    }, () => {
      this.skipTurn()
    })
  }

  // Переадресация хода на следующего игрока
  // returns : игровое поле
  skipTurn(squares) {
    let turn = this.state.currentPlayer
    let winner = false;

    for (let i = 1; i < 4; i++) {
      if (!this.state.dead[(this.state.currentPlayer + i) % 4]) {
        turn = i + this.state.currentPlayer
        break
      }
    }
    let deadMeat = true, deadAI = true
    for (let i = 0; i < 4; i++) {
      if (!this.state.dead[i]) {
        if (this.state.meat[i]) {
          deadMeat = false
        } else {
          deadAI = false
        }
      }
    }
    
    if (turn === this.state.currentPlayer) {
      this.gameOver("Game over")
    }

    this.setState({
      currentPlayer: turn
    }, () => {
      if (!this.state.meat[turn % 4]) {
        this.useSkynet()
      }
    })

    if (squares) {
      return this.reverseSelection(squares)
    } else {
      this.setState({
        handling: false,
        squares: this.validate(this.reverseSelection(this.state.squares))
      })
    }
  }

  // Отмена выбора шашки персонажа и очистка хайлайтов
  // returns : игровое поле
  reverseSelection(squares) {
    return squares.map(x => x ? x.toUpperCase() : null).map(x => x === 'V' ? null : x)
  }


  // Выставляем хайлайты для шашек
  // returns : игровое поле
  checkMovement(squares, i, lock) {
    // Достаем полный номинал выбранной игроком шашки
    let check = squares[i].toUpperCase()
    // Смотрим, является ли она дамкой
    if (check.endsWith('Q')) {
      // Влево
      for (let q = i - 1; q >= Math.floor(i / 6) * 6; q--) {
        if (squares[q] !== null) {
          if (squares[q].charAt(0) !== check.charAt(0) && squares[q - 1] === null && q !== Math.floor(i % 6) * 6) {
            squares[q - 1] = 'v'
          }
          break
        } else {
          if (!lock)
            squares[q] = 'v'
        }
      }
      // Вправо
      for (let q = i + 1; q <= Math.floor(i / 6) * 6 + 5; q++) {
        if (squares[q] !== null) {
          if (squares[q].charAt(0) !== check.charAt(0) && squares[q + 1] === null && q !== Math.floor(i % 6) * 6 + 5) {
            squares[q + 1] = 'v'
          }
          break
        } else {
          if (!lock)
            squares[q] = 'v'
        }
      }
      // Вверх
      for (let q = i - 6; q > 0; q -= 6) {
        if (squares[q] !== null) {
          if (squares[q].charAt(0) !== check.charAt(0) && squares[q - 6] === null && q !== i % 6) {
            squares[q - 6] = 'v'
          }
          break
        } else {
          if (!lock)
            squares[q] = 'v'
        }
      }
      // Вниз
      for (let q = i + 6; q < 36; q += 6) {
        if (squares[q] !== null) {
          if (squares[q].charAt(0) !== check.charAt(0) && squares[q + 6] === null && q !== 30 + i % 6) {
            squares[q + 6] = 'v'
          }
          break
        } else {
          if (!lock)
            squares[q] = 'v'
        }
      }

    } else {
      // Вправо
      if (squares[i + 1]) {
        if (squares[i + 2] === null && check.charAt(0) !== squares[i + 1].charAt(0) && Math.floor((i + 2) / 6) === Math.floor(i / 6)) {
          squares[i + 2] = 'v'
        }
      }
      else {
        if (!lock && i < 35 && Math.floor((i + 1) / 6) === Math.floor(i / 6) && (this.state.currentPlayer % 4 === 0 || this.state.currentPlayer % 4 === 3))
          squares[i + 1] = 'v'
      }

      // Вниз
      if (squares[i + 6]) {
        if (squares[i + 12] === null && check.charAt(0) !== squares[i + 6].charAt(0) && i + 6 < 30 && i + 6 > 5) {
          squares[i + 12] = 'v'
        }
      }
      else {
        if (!lock && i < 30 && (this.state.currentPlayer % 4 === 0 || this.state.currentPlayer % 4 === 1))
          squares[i + 6] = 'v'
      }

      // Влево
      if (squares[i - 1]) {
        if (squares[i - 2] === null && check.charAt(0) !== squares[i - 1].charAt(0) && Math.floor((i - 2) / 6) === Math.floor(i / 6)) {
          squares[i - 2] = 'v'
        }
      }
      else {
        if (!lock && i > 0 && Math.floor((i - 1) / 6) === Math.floor(i / 6) && (this.state.currentPlayer % 4 === 1 || this.state.currentPlayer % 4 === 2))
          squares[i - 1] = 'v'
      }

      // Вверх
      if (squares[i - 6]) {
        if (squares[i - 12] === null && check.charAt(0) !== squares[i - 6].charAt(0) && i - 6 < 30 && i - 6 > 5) {
          squares[i - 12] = 'v'
        }
      }
      else {
        if (!lock && i > 5 && (this.state.currentPlayer % 4 === 2 || this.state.currentPlayer % 4 === 3))
          squares[i - 6] = 'v'
      }
    }

    return squares
  }

  
  // Рендер
  render() {
    return (
      <>
        <Text>{UserName.userName} is white, bot is green</Text>
        <Text></Text>      
        {/* Игровое поле */}
        {(!this.state.waiting || this.state.spectator) && <View className="game-board">
          {setBackground(this.state.currentPlayer)}
          <Board
            squares={this.state.squares}
            onClick={i => this.handleClick(i)}
          />
        </View>
        }
        {/* Кнопки */}
        <View className="game-info" style={{ flexDirection: 'row' }}>
          {/* Кнопка краткосрочной ликвидации */}
          {<TouchableOpacity onPress={() => this.giveUp()} style={styles.button} disabled={this.state.disableButtons} >
            <Text style={styles.text}>Give Up</Text>
          </TouchableOpacity>
          }
          {/* Кнопка пропуска хода */}
          {<TouchableOpacity onPress={() => this.skipTurn(null)} style={styles.button} disabled={this.state.disableButtons}>
            <Text style={styles.text}>Skip Turn</Text>
          </TouchableOpacity>
          }
          {/* Кнопка ресета игры */}
          {<TouchableOpacity onPress={() => this.reset()} style={styles.imageButton} disabled={this.state.disableButtons}>
            <Image style={{ flex: 1, alignContent: 'center', width: 40, height: undefined, resizeMode: 'contain', }} source={require('./refresh.png')} />
          </TouchableOpacity>
          }
        </View>
      </>
    );
  }
}

// Элемент, меняющий фон под нынешнего игрока
function setBackground(params) {
  switch (params % 4) {
    case 0:
      return <View style={[styles.turnBackground, { backgroundColor: 'rgba(255,255,255,1)', }]}></View>

    case 1:
      return <View style={[styles.turnBackground, { backgroundColor: 'rgba(255,0,0,1)', }]}></View>

    case 2:
      return <View style={[styles.turnBackground, { backgroundColor: 'rgba(0,255,0,1)', }]}></View>

    case 3:
      return <View style={[styles.turnBackground, { backgroundColor: 'rgba(50,50,50,1)' }]}></View>

    default:
      return <View></View>;
  }
  return <View style={{ position: 'absolute', width: '400px', height: '400px', margin: '0px', backgroundColor: {}, }}></View>
}

// Стили
const styles = StyleSheet.create({
  turnBackground: {
    position: 'absolute',
    width: 345,
    height: 345,
    margin: 0
  },
  button: {
    alignItems: 'center',
    justifyContent: 'center',
    margin: 8,
    paddingVertical: 12,
    paddingHorizontal: 24,
    borderRadius: 4,
    elevation: 3,
    backgroundColor: 'black',
  },
  info: {
    alignItems: 'center',
    justifyContent: 'center',
    margin: 10,
    paddingVertical: 12,
    paddingHorizontal: 28,
    borderRadius: 4,
    elevation: 3,
    backgroundColor: 'black',
    opacity: 0.6
  },
  imageButton: {
    alignItems: 'center',
    justifyContent: 'center',
    margin: 10,
    padding: 5,
    borderRadius: 4,
    elevation: 3,
    backgroundColor: 'black',
  },
  text: {
    fontSize: 16,
    lineHeight: 21,
    fontWeight: 'bold',
    letterSpacing: 0.25,
    color: 'white',
  },
});