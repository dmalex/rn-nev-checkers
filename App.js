
import { StyleSheet, Text, TouchableOpacity, Image } from 'react-native';
import { Game } from './Game'
import React from 'react'
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { LinearGradient } from 'expo-linear-gradient'
import { Selection } from './Selection';
import { UserSettings } from './Settings.js';

function HomeScreen({ navigation }) {
  return (
    <>
      <LinearGradient start={{ x: 0, y: 0.75 }} end={{ x: 1, y: 0.25 }} colors={['#1FA2FF', '#12D8FA', '#A6FFCB']} style={{ flex: 1, alignItems: 'center', justifyContent: 'center', }}>
        <TouchableOpacity style={styles.button} onPress={() => navigation.navigate('User vs bot')}>
          <Text style={styles.text}>
            Play
          </Text>
        </TouchableOpacity>
      </LinearGradient>

      <TouchableOpacity style={styles.button_corner} onPress={() => navigation.navigate('Settings')}>
        <Image style={{ flex: 1, alignContent: 'center', width: 40, height: undefined, resizeMode: 'contain', }} source={require('./setting.png')} />
      </TouchableOpacity>
    </>
  );
}

function PlayScreen({ navigation }) {
  return (
    <LinearGradient start={{ x: 0, y: 0.75 }} end={{ x: 1, y: 0.25 }} colors={['#7841fa', '#4df7ec', '#3afc61']} style={{ flex: 1, alignItems: 'center', justifyContent: 'center', }}>
      <Game />
    </LinearGradient>
  );
}

function SettingsScreen() {
  return <LinearGradient start={{ x: 0, y: 0.75 }} end={{ x: 1, y: 0.25 }} colors={['#12c2e9', '#c471ed', '#f64f59']} style={{ flex: 1, alignItems: 'center', justifyContent: 'center', }}>
    <Selection>
    </Selection>
  </LinearGradient>
}

const Stack = createNativeStackNavigator();

export default function App() {
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName="Nevsky checkers">
        <Stack.Screen name="Nevsky checkers" component={HomeScreen} options={{
          headerTransparent: true, animationTypeForReplace: 'pop',
          animation: 'fade'
        }} />
        <Stack.Screen name="User vs bot" component={PlayScreen} options={{
          headerTransparent: true, animationTypeForReplace: 'pop',
          animation: 'fade'
        }} />
        <Stack.Screen name="Settings" component={SettingsScreen} options={{
          headerTransparent: true, animationTypeForReplace: 'pop',
          animation: 'fade'
        }} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}


const styles = StyleSheet.create({
  linearGradient: {
    flex: 1,
    paddingLeft: 15,
    paddingRight: 15,
    borderRadius: 5
  },
  buttonText: {
    fontSize: 18,
    fontFamily: 'Gill Sans',
    textAlign: 'center',
    margin: 10,
    color: '#ffffff',
    backgroundColor: 'transparent',
  },
  turnBackground: {
    position: 'absolute',
    width: 425,
    height: 425,
    margin: 0
  },
  button: {
    alignItems: 'center',
    justifyContent: 'center',
    margin: 6,
    paddingVertical: 10,
    paddingHorizontal: 28,
    borderRadius: 4,
    elevation: 3,
    backgroundColor: 'black',
  },
  button_corner: {
    maxHeight: 40,
    maxWidth: 40,
    height: 40,
    width: 40,
    alignItems: 'center',
    justifyContent: 'center',
    margin: 10,
    padding: 5,
    borderRadius: 4,
    elevation: 3,
    backgroundColor: 'black',
    position: 'absolute',
    bottom: 10,
    left: 10,
    // zIndex: 9,
  },
  imageButton: {
    alignItems: 'center',
    justifyContent: 'center',
    margin: 10,
    padding: 5,
    borderRadius: 4,
    elevation: 3,
    backgroundColor: 'black',
  },
  text: {
    fontSize: 16,
    lineHeight: 21,
    fontWeight: 'bold',
    letterSpacing: 0.25,
    color: 'white',
  },
  container: {
    margin: 0,
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'linear-gradient(to bottom, #6be569, #6d92d2)',
  },
  linearGradient: {
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 5,
    height: 500,
    width: 500,
  },
});
